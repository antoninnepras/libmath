CC	= gcc
CFLAGS	= -Wall -g

LIB_NAME = math
LIB_ARCHIVE = lib/lib$(LIB_NAME).a
LIB_FILES = $(wildcard src/*.c)
LIB_OBJECTS = $(patsubst %.c,%.o,$(LIB_FILES))
LIB_HEADERS = $(wildcard src/*.h)
INCLUDE_HEADERS = $(patsubst src/%,include/%,$(LIB_HEADERS))

LFLAGS = -I include -L lib

$(LIB_ARCHIVE): $(LIB_OBJECTS) $(LIB_HEADERS)
	mkdir -p lib include
	ar cr lib/lib$(LIB_NAME).a src/*.o
	cp $(LIB_HEADERS) include


src/%.o: src/%.c src/%.h
	$(CC) -c -o $@ $< $(CFLAGS) 

	
clean:
	rm -f $(LIB_ARCHIVE) $(LIB_OBJECTS) $(INCLUDE_HEADERS)
	
