#ifndef __math_h__
#define __math_h__

#include "dyn_matrix.h"
#include "mat22.h"
#include "mat33.h"
#include "mat44.h"
#include "triangle2.h"
#include "triangle3.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"

#endif
